﻿// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PingPong : ModuleRules
{
	public PingPong(TargetInfo Target)
	{
        PublicIncludePaths.Add("Runtime/Launch/Public");
        PublicIncludePaths.Add("Runtime/Launch/Private");// For LaunchEngineLoop.cpp, to be included correctly from RequiredProgramMainCPPInclude.h

        PrivateIncludePathModuleNames.AddRange(
            new string[] {
				        "Messaging",
                        "EngineMessages",
			        }
        );

        PrivateDependencyModuleNames.AddRange(
            new string[] {
				    "Core",
                    "Projects",
                    "Messaging",
                    "EngineMessages",
                    "UdpMessaging",
			    }
        );
    }
}
