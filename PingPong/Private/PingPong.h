// Copyright 2014 Sebastian Kowalczyk, Inc. All Rights Reserved.

#pragma once

#include "Core.h"
#include "CoreUObject.h"
#include "EngineServiceMessages.h"
#include "Messaging.h"
#include "ConsoleApplication.h"
#include "PingPong.generated.h"


USTRUCT()
struct FPingPongStartEvent
{
	GENERATED_USTRUCT_BODY()
};

USTRUCT()
struct FPingPongReadyEvent
{
	GENERATED_USTRUCT_BODY()
};

USTRUCT()
struct FPingPongQuitEvent
{
	GENERATED_USTRUCT_BODY()
};

USTRUCT()
struct FPingPongMessage
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FString Message;

	FPingPongMessage() {}
	FPingPongMessage(const FString& InMessage) : Message(InMessage) {}
};

namespace EAppMode
{
	enum Type
	{
		Server,
		Client,
		Standalone,
	};

	FString ToString(EAppMode::Type InMode);
}

class FMessageBusHandler
{
public:
	FMessageBusHandler(EAppMode::Type InMode);

	void Startup();
	void Shutdown();

	/** Callback for handling FPingPongQuitEvent messages. */
	void HandleQuitMessage(const FPingPongQuitEvent& Message, const IMessageContextRef& Context);

	void HandleStartMessage(const FPingPongStartEvent& Message, const IMessageContextRef& Context);

	void HandleMessage(const FPingPongMessage& Message, const IMessageContextRef& Context);

	void HandleReadyMessage(const FPingPongReadyEvent& Message, const IMessageContextRef& Context);

protected:
	IMessageBusWeakPtr MessageBusPtr;
	FMessageEndpointPtr ServerMessageEndpoint;
	FMessageEndpointPtr ClientMessageEndpoint;
	const EAppMode::Type AppMode;
};

struct FPingPongApplication : public FConsoleApplication
{
public:
	FPingPongApplication();

	virtual void Startup() override;
	virtual int32 ShutDown() override;

protected:
	virtual void LoadModules() override;

protected:
	TSharedPtr<FMessageBusHandler> UMBHandler;
	EAppMode::Type Mode;
};
