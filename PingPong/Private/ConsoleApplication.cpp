// Copyright 2014 Sebastian Kowalczyk, Inc. All Rights Reserved.
#include "PingPong.h"
#include "ConsoleApplication.h"
#include "LaunchEngineLoop.h"

DEFINE_LOG_CATEGORY_STATIC(LogConsoleApp, Log, All);

FConsoleApplication::FConsoleApplication()
	: MainsFramerate(60.0f)
	, BatteryFramerate(30.0f)
	, InactiveFramerate(5.0f)
	, MainsFrameTime(1.0f / MainsFramerate)
	, BatteryFrameTime(1.0f / BatteryFramerate)
	, BackgroundFrameTime(1.0f / InactiveFramerate)
	, DeltaTime(0.0)
	, LastTime(FPlatformTime::Seconds())
{

}

FConsoleApplication::~FConsoleApplication()
{

}

void FConsoleApplication::LoadModules()
{

}

void FConsoleApplication::AppTick() 
{

}

void FConsoleApplication::Startup()
{
	FModuleManager::Get().StartProcessingNewlyLoadedObjects();
	this->LoadModules();
	ProcessNewlyLoadedUObjects();

	// Setup desired frame times
	GConfig->GetFloat(TEXT("FramerateThrottle"), TEXT("Mains"), MainsFramerate, GEngineIni);
	MainsFramerate = FMath::Clamp<float>(MainsFramerate, 1, 60);
	GConfig->GetFloat(TEXT("FramerateThrottle"), TEXT("Battery"), BatteryFramerate, GEngineIni);
	BatteryFramerate = FMath::Clamp<float>(BatteryFramerate, 1, 60);
	GConfig->GetFloat(TEXT("FramerateThrottle"), TEXT("Inactive"), InactiveFramerate, GEngineIni);
	InactiveFramerate = FMath::Clamp<float>(InactiveFramerate, 1, 60);

	MainsFrameTime = 1.0f / MainsFramerate;
	BatteryFrameTime = 1.0f / BatteryFramerate;
	BackgroundFrameTime = 1.0f / InactiveFramerate;
}

int32 FConsoleApplication::ShutDown()
{
	// shut down 
	FEngineLoop::AppPreExit();
	FModuleManager::Get().UnloadModulesAtShutdown();

	FTaskGraphInterface::Shutdown();
	FEngineLoop::AppExit();

	return 0;
}

void FConsoleApplication::UpdateSubSystems()
{
	// update sub-systems
	FTaskGraphInterface::Get().ProcessThreadUntilIdle(ENamedThreads::GameThread);
	FTicker::GetCoreTicker().Tick(DeltaTime);
}

void FConsoleApplication::ThrottleFrameRate()
{
	// throttle frame rate
	const bool bAppIsFocused = FPlatformProcess::IsThisApplicationForeground();
	const bool bIsOnBattery = FPlatformMisc::IsRunningOnBattery();
	const float FrameTime = bAppIsFocused ? bIsOnBattery ? BatteryFrameTime : MainsFrameTime : BackgroundFrameTime;
	FPlatformProcess::Sleep(FMath::Max<float>(0.0f, FrameTime - (FPlatformTime::Seconds() - LastTime)));

	// calculate deltas
	const double AppTime = FPlatformTime::Seconds();
	DeltaTime = AppTime - LastTime;
	LastTime = AppTime;
}

void FConsoleApplication::RunLoop()
{
	while (!GIsRequestingExit)
	{
		UpdateSubSystems();

		// do some app per tick things here
		AppTick();

		if (GLog)
		{
			GLog->FlushThreadedLogs();
		}
		ThrottleFrameRate();
	}
}
