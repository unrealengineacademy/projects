// Copyright 2014 Sebastian Kowalczyk, Inc. All Rights Reserved.

#pragma once

#include "Core.h"
#include "CoreUObject.h"

struct FConsoleApplication
{
public:
	FConsoleApplication();
	virtual ~FConsoleApplication();

	virtual void Startup();
	virtual int32 ShutDown();
	virtual void RunLoop();

protected:
	virtual void LoadModules();
	virtual void AppTick();
	void UpdateSubSystems();
	void ThrottleFrameRate();

protected:
	float MainsFramerate;
	float BatteryFramerate;
	float InactiveFramerate;

	float MainsFrameTime;
	float BatteryFrameTime;
	float BackgroundFrameTime;

	float DeltaTime;
	float LastTime;
};
