// Copyright 2014 Sebastian Kowalczyk, Inc. All Rights Reserved.

#include "PingPong.h"
#include "RequiredProgramMainCPPInclude.h"

DEFINE_LOG_CATEGORY_STATIC(LogPingPong, Log, All);
IMPLEMENT_APPLICATION(PingPong, "PingPong");

INT32_MAIN_INT32_ARGC_TCHAR_ARGV()
{
	check(GEngineLoop.PreInit(ArgC, ArgV, TEXT(" -Messaging")) != -1);
	FPingPongApplication App;

	App.Startup();

	App.RunLoop();

	return App.ShutDown();
}

namespace EAppMode
{
	FString ToString(EAppMode::Type InMode)
	{
		FString ModeName = TEXT("Standalone");

		switch (InMode)
		{
		case EAppMode::Server:
			ModeName = TEXT("Server");
			break;
		case EAppMode::Client:
			ModeName = TEXT("Client");
			break;
		case EAppMode::Standalone:
		default:
			ModeName = TEXT("Standalone");
			break;
		}

		return ModeName;
	}
}

FMessageBusHandler::FMessageBusHandler(EAppMode::Type InMode)
	: AppMode(InMode)
{
}

void FMessageBusHandler::Startup()
{
	MessageBusPtr = IMessagingModule::Get().GetDefaultBus();
	check(MessageBusPtr.IsValid());

	if (AppMode == EAppMode::Server || AppMode == EAppMode::Standalone)
	{
		ServerMessageEndpoint = FMessageEndpoint::Builder("FMessageBusHandler", MessageBusPtr.Pin().ToSharedRef())
			.Handling<FPingPongStartEvent>(this, &FMessageBusHandler::HandleStartMessage)
			.Handling<FPingPongQuitEvent>(this, &FMessageBusHandler::HandleQuitMessage)
			.Handling<FPingPongMessage>(this, &FMessageBusHandler::HandleMessage)
			.ReceivingOnThread(ENamedThreads::AnyThread);

		if (ServerMessageEndpoint.IsValid())
		{
			ServerMessageEndpoint->Subscribe<FPingPongStartEvent>();
			ServerMessageEndpoint->Subscribe<FPingPongQuitEvent>();
			ServerMessageEndpoint->Subscribe<FPingPongMessage>();
		}
	}

	if (AppMode == EAppMode::Client || AppMode == EAppMode::Standalone)
	{
		ClientMessageEndpoint = FMessageEndpoint::Builder("FMessageBusHandler", MessageBusPtr.Pin().ToSharedRef())
			.Handling<FPingPongQuitEvent>(this, &FMessageBusHandler::HandleQuitMessage)
			.Handling<FPingPongReadyEvent>(this, &FMessageBusHandler::HandleReadyMessage)
			.ReceivingOnThread(ENamedThreads::AnyThread);

		if (ClientMessageEndpoint.IsValid())
		{
			ClientMessageEndpoint->Subscribe<FPingPongReadyEvent>();
			if (AppMode != EAppMode::Standalone)
			{
				ClientMessageEndpoint->Subscribe<FPingPongQuitEvent>();
			}
		}

		if (ClientMessageEndpoint.IsValid())
		{
			ClientMessageEndpoint->Publish(new FPingPongStartEvent(), EMessageScope::All);
		}
	}
}

void FMessageBusHandler::Shutdown()
{
	MessageBusPtr.Reset();
}

void FMessageBusHandler::HandleReadyMessage(const FPingPongReadyEvent& Message, const IMessageContextRef& Context)
{
	ClientMessageEndpoint->Publish(new FPingPongMessage(TEXT("Test message with UMB")), EMessageScope::All);
}

void FMessageBusHandler::HandleStartMessage(const FPingPongStartEvent& Message, const IMessageContextRef& Context)
{
	ServerMessageEndpoint->Publish(new FPingPongReadyEvent(), EMessageScope::All);
}

void FMessageBusHandler::HandleQuitMessage(const FPingPongQuitEvent& Message, const IMessageContextRef& Context)
{
	UE_LOG(LogPingPong, Warning, TEXT("Got quit event!"));
	FPlatformMisc::RequestExit(false);
}

void FMessageBusHandler::HandleMessage(const FPingPongMessage& Message, const IMessageContextRef& Context)
{
	UE_LOG(LogPingPong, Display, TEXT("%s"), *Message.Message);
	ServerMessageEndpoint->Publish(new FPingPongQuitEvent(), EMessageScope::All);
}


FPingPongApplication::FPingPongApplication() : FConsoleApplication(), Mode(EAppMode::Standalone) { }

void FPingPongApplication::LoadModules()
{
	FConsoleApplication::LoadModules();
	//Load UDP plugin for UMB
	FModuleManager::Get().LoadModule("UdpMessaging");
}

void FPingPongApplication::Startup()
{
	FConsoleApplication::Startup();

	const bool RunAsServer = FParse::Param(FCommandLine::Get(), TEXT("RunServer"));
	const bool RunAsClient = !RunAsServer && FParse::Param(FCommandLine::Get(), TEXT("RunClient"));

	Mode = RunAsServer ? EAppMode::Server : (RunAsClient ? EAppMode::Client : EAppMode::Standalone);
	UE_LOG(LogPingPong, Display, TEXT("PingPong started as %s\n"), *EAppMode::ToString(Mode));

	UMBHandler = MakeShareable(new FMessageBusHandler(Mode));
	check(UMBHandler.IsValid());
	UMBHandler->Startup();
}

int32 FPingPongApplication::ShutDown()
{
	FConsoleApplication::ShutDown();

	UMBHandler->Shutdown();
	UMBHandler.Reset();

	return 0;
}
