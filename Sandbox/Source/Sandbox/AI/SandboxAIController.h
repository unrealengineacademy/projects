// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "BehaviorTree/BehaviorTreeTypes.h"
#include "Perception/AISense.h"
#include "SandboxTeamActorInterface.h"
#include "SandboxAIController.generated.h"

class UAISenseConfig;
class UBlackboardComponent;
class UBlackboardData;

namespace FBlackboard
{
	const FName KeyGoalActor = TEXT("GoalActor");
	const FName KeyTargetActor = TEXT("TargetActor");
}

/**
*
*/
UCLASS()
class SANDBOX_API ASandboxAIController : public AAIController, public ISandboxTeamActorInterface
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditDefaultsOnly, Instanced, Category = "Perception")
	TArray<UAISenseConfig*> DefaultSenseConfigurations;

	UPROPERTY(EditDefaultsOnly, Category = "Perception")
	TSubclassOf<UAISense> DominantSense;

	virtual bool InitializeBlackboard(UBlackboardComponent& BlackboardComp, UBlackboardData& BlackboardAsset) override;

	/** Overridden to return actual team number */
	virtual TEnumAsByte<ESandboxTeam::Type> GetTeam() const override;

	UFUNCTION(BlueprintCallable, Category = "Team")
	void SetTeam(TEnumAsByte<ESandboxTeam::Type> InTeam);

	UFUNCTION(BlueprintImplementableEvent, Category = "Team")
	void OnSetTeam(ESandboxTeam::Type NewTeam);

	// IGenericTeamAgentInterface
	virtual void SetGenericTeamId(const FGenericTeamId& TemaID) override;

	UFUNCTION(BlueprintCallable, Category = "AI")
	virtual void SetGoalActor(AActor* InGoalActor);

	UFUNCTION(BlueprintCallable, Category = "AI")
	AActor* GetGoalActor() { return GoalActorCached.Get(); }

	UFUNCTION(BlueprintCallable, Category = "AI")
	virtual void SetTargetActor(AActor* InTargetActor);
	
	UFUNCTION(BlueprintCallable, Category = "AI")
	AActor* GetTargetActor() { return TargetActorCached.Get(); }

protected:
	ESandboxTeam::Type Team;
	FBlackboard::FKey BlackboardKey_GoalActor;
	FBlackboard::FKey BlackboardKey_TargetActor;

	TWeakObjectPtr<AActor> GoalActorCached;
	TWeakObjectPtr<AActor> TargetActorCached;


};
