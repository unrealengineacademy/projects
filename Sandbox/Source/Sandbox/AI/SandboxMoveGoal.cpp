#include "Sandbox.h"
#include "Components/BillboardComponent.h"
#include "EnvironmentQuery/EnvQuery.h"
#include "EnvironmentQuery/EQSTestingPawn.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "EnvironmentQuery/EQSRenderingComponent.h"
#include "SandboxMoveGoal.h"

ASandboxMoveGoal::ASandboxMoveGoal(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer), bMoveToRandomLocation(false), Radius(100), AssignTime(2)
{
	if (!HasAnyFlags(RF_ClassDefaultObject))
	{
		DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DummyRoot"));
		RootComponent = DummyRoot;

		// Structure to hold one-time initialization
		struct FConstructorStatics
		{
			ConstructorHelpers::FObjectFinderOptional<UTexture2D> IconTexture;
			FName ID_Misc;
			FText NAME_Misc;
			FConstructorStatics()
				: IconTexture(TEXT("Texture2D'/Engine/EditorResources/Goal_Waypoint.Goal_Waypoint'"))
				, ID_Misc(TEXT("Misc"))
			{
			}
		};
		static FConstructorStatics ConstructorStatics;

		UBillboardComponent* SpriteComponent = CreateEditorOnlyDefaultSubobject<UBillboardComponent>(TEXT("SpriteBillboar"));
		if (SpriteComponent)
		{
			SpriteComponent->Sprite = ConstructorStatics.IconTexture.Get();
			SpriteComponent->bHiddenInGame = true;
			SpriteComponent->SetRelativeLocation(FVector(0, 0, 30));
			SpriteComponent->AttachParent = RootComponent;
		}

		USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
		static FName CollisionProfileName(TEXT("OverlapOnlyPawn"));
		SphereComponent->SetCollisionProfileName(CollisionProfileName);
		SphereComponent->bHiddenInGame = true;
		SphereComponent->AttachParent = RootComponent;
		SphereComponent->CanCharacterStepUpOn = ECB_No;
		SphereComponent->bShouldUpdatePhysicsVolume = true;
		SphereComponent->bCanEverAffectNavigation = false;
		SphereComponent->bDynamicObstacle = false;
		SphereComponent->SetSphereRadius(Radius);
	}
}

/**
*	Retrieves Agent's location
*/
FVector ASandboxMoveGoal::GetNavAgentLocation() const
{
	return GetActorLocation();
}

/** Allow actor to specify additional offset (relative to NavLocation) when it's used as move goal */
FVector ASandboxMoveGoal::GetMoveGoalOffset(AActor* MovingActor) const 
{ 
	if (bMoveToRandomLocation == false)
	{
		return FVector::ZeroVector;
	}

	if (OffsetsCache.Contains(MovingActor) == false)
	{
		OffsetsCache.Add(MovingActor, FGoalOffsetData(FVector(FMath::FRandRange(-Radius, Radius), FMath::FRandRange(-Radius, Radius), 0), GetWorld()->TimeSeconds, AssignTime));
	}
	else
	{
		FGoalOffsetData &OffsetData = OffsetsCache[MovingActor];
		if (bRandomMovementAroundGoal && OffsetData.Time + OffsetData.AssignTime < GetWorld()->GetTimeSeconds())
		{
			FVector NewOffset = FVector(FMath::FRandRange(-Radius, Radius), FMath::FRandRange(-Radius, Radius), 0);
			OffsetsCache[MovingActor] = FGoalOffsetData(NewOffset, GetWorld()->TimeSeconds, AssignTime);
		}
	}

	return OffsetsCache[MovingActor].Offset;
}

void ASandboxMoveGoal::GetMoveGoalReachTest(class AActor* MovingActor, const FVector& MoveOffset, FVector& GoalOffset, float& GoalRadius, float& GoalHalfHeight) const
{
	GoalOffset = OffsetsCache.Contains(MovingActor) == true ? OffsetsCache[MovingActor].Offset : GetMoveGoalOffset(MovingActor);
	GetSimpleCollisionCylinder(GoalRadius, GoalHalfHeight);
	GoalRadius = 20; //FIX ME: It can't be hard coded here.
}

#if WITH_EDITOR
void ASandboxMoveGoal::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	static const FName NAME_Radius = GET_MEMBER_NAME_CHECKED(ASandboxMoveGoal, Radius);

	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (PropertyChangedEvent.Property)
	{
		if (PropertyChangedEvent.Property->GetFName() == NAME_Radius)
		{
			USphereComponent* SphereComponent = FindComponentByClass<USphereComponent>();
			SphereComponent->SetSphereRadius(Radius);
		}
	}
}
#endif