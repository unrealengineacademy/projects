// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#include "SandboxTeamActorInterface.h"
#include "SandboxMoveGoal.generated.h"

struct FGoalOffsetData
{
	FGoalOffsetData() : Offset(FVector::ZeroVector), Time(0), AssignTime(-1) {}
	FGoalOffsetData(const FVector& InOffset, float InTime, float InAssignTime) : Offset(InOffset), Time(InTime), AssignTime(InAssignTime) {}

	FVector Offset;
	float Time;
	float AssignTime;
};

class UEnvQuery;

UCLASS(hidecategories = (Object, Actor, Input, Rendering), Blueprintable, Abstract)
class ASandboxMoveGoal : public AActor, public INavAgentInterface
{
	GENERATED_UCLASS_BODY()

	UPROPERTY()
	class USceneComponent* DummyRoot;

	/**
	*	Retrieves Agent's location
	*/
	virtual FVector GetNavAgentLocation() const;

	/** Allow actor to specify additional offset (relative to NavLocation) when it's used as move goal */
	virtual FVector GetMoveGoalOffset(AActor* MovingActor) const;

	virtual void GetMoveGoalReachTest(AActor* MovingActor, const FVector& MoveOffset, FVector& GoalOffset, float& GoalRadius, float& GoalHalfHeight) const;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(Category=Goal))
	bool bMoveToRandomLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Category = Goal))
	float Radius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Category = Goal))
	float AssignTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Category = Goal))
	ASandboxMoveGoal* LinkedNextGoal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Category = RandomMovement))
	bool bRandomMovementAroundGoal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = bRandomMovementAroundGoal, Category = RandomMovement))
	float StandingTimeout;

	mutable TMap<AActor*, FGoalOffsetData> OffsetsCache;
};
