#pragma once
#include "Engine/DataTable.h"
#include "SandboxAITypes.generated.h"

UENUM()
namespace EDiscreteDistance
{
	enum Type
	{
		Melee,
		Close,
		FarAway,
	};
}
