// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Sandbox.h"
#include "SandboxAICharacter.h"

//////////////////////////////////////////////////////////////////////////
// AMadnessCharacter

ASandboxAICharacter::ASandboxAICharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bCanBeDamaged = true;
}

