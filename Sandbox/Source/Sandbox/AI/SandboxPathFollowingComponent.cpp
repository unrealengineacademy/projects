// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Sandbox.h"
#include "SandboxPathFollowingComponent.h"
#include "SandboxAIController.h"

//----------------------------------------------------------------------//
// UFortPathFollowingComponent
//----------------------------------------------------------------------//
USandboxPathFollowingComponent::USandboxPathFollowingComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// give more freedom on height test, allow difference = [agent half height] * 1.5 
	// (navmesh clipping through geometry can result in path points being slightly underground)
	MinAgentHalfHeightPct = 2.00f;
	bUseVisibilityTestsSimplification = true;
}

void USandboxPathFollowingComponent::Initialize()
{
	Super::Initialize();

	MyAI = Cast<ASandboxAIController>(GetOwner());
}

FVector USandboxPathFollowingComponent::GetMoveFocus(bool bAllowStrafe) const
{
	const FVector FocalPoint = bAllowStrafe ? MyAI->GetFocalPoint() : FAISystem::InvalidLocation;
	if (FocalPoint != FAISystem::InvalidLocation)
	{
		return FocalPoint;
	}

	return Super::GetMoveFocus(bAllowStrafe);
}