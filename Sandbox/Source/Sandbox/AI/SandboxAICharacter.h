// Copyright Sebastian Kowalczyk. All Rights Reserved.
#pragma once

#include "GameFramework/Character.h"
#include "SandboxAICharacter.generated.h"

UCLASS(config = Game, Abstract)
class SANDBOX_API ASandboxAICharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()
};
