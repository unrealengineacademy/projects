// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "AITypes.h"
#include "Navigation/PathFollowingComponent.h"
#include "SandboxPathFollowingComponent.generated.h"

class ASandboxAIController;

UCLASS(config = Game, BlueprintType)
class USandboxPathFollowingComponent : public UPathFollowingComponent
{
	GENERATED_UCLASS_BODY()

	/** Cached AI controller, most common owner */
	UPROPERTY()
	ASandboxAIController* MyAI;

	// UPathFollowingComponent BEGIN
	virtual void Initialize() override;
	// UPathFollowingComponent END

	virtual FVector GetMoveFocus(bool bAllowStrafe) const override;
};