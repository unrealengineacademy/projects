// Fill out your copyright notice in the Description page of Project Settings.

#include "Sandbox.h"
#include "Perception/AIPerceptionComponent.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "SandboxAIController.h"
#include "SandboxPathFollowingComponent.h"

ASandboxAIController::ASandboxAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<USandboxPathFollowingComponent>(TEXT("PathFollowingComponent")))
	, Team(ESandboxTeam::MAX)
{
	UAIPerceptionComponent* PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));
	if (PerceptionComponent)
	{
		SetPerceptionComponent(*PerceptionComponent);

		for (auto Config : DefaultSenseConfigurations)
		{
			PerceptionComponent->ConfigureSense(*Config);
		}

		PerceptionComponent->SetDominantSense(DominantSense);
	}

	Blackboard = CreateDefaultSubobject<UBlackboardComponent>("BlackboardComp");
	BlackboardKey_GoalActor = MAX_uint8;
	BlackboardKey_TargetActor = MAX_uint8;
}

bool ASandboxAIController::InitializeBlackboard(UBlackboardComponent& BlackboardComp, UBlackboardData& BlackboardAsset)
{
	if (Super::InitializeBlackboard(BlackboardComp, BlackboardAsset))
	{
		check(&BlackboardComp == Blackboard);

		BlackboardKey_GoalActor = BlackboardAsset.GetKeyID(FBlackboard::KeyGoalActor);
		BlackboardKey_TargetActor = BlackboardAsset.GetKeyID(FBlackboard::KeyTargetActor);

		// FBlackboard::KeySelf initialized in AAIController
		Blackboard->SetValue<UBlackboardKeyType_Object>(BlackboardKey_GoalActor, GetGoalActor());
		Blackboard->SetValue<UBlackboardKeyType_Object>(BlackboardKey_TargetActor, GetTargetActor());

		return true;
	}

	return false;
}

void ASandboxAIController::SetGenericTeamId(const FGenericTeamId& TemaID)
{
	SetTeam(ESandboxTeam::Type(uint8(TemaID)));
}

TEnumAsByte<ESandboxTeam::Type> ASandboxAIController::GetTeam() const
{
	return Team;
}

void ASandboxAIController::SetTeam(TEnumAsByte<ESandboxTeam::Type> InTeam)
{
	if (Team != InTeam)
	{
		Team = InTeam;

		if (GetAIPerceptionComponent())
		{
			GetAIPerceptionComponent()->RequestStimuliListenerUpdate();
		}

		OnSetTeam(InTeam);
	}
}

void ASandboxAIController::SetGoalActor(AActor* InGoalActor)
{
	if (Blackboard)
	{
		Blackboard->SetValue<UBlackboardKeyType_Object>(BlackboardKey_GoalActor, InGoalActor);
	}
	GoalActorCached = InGoalActor;
}

void ASandboxAIController::SetTargetActor(AActor* InTargetActor)
{
	if (Blackboard)
	{
		Blackboard->SetValue<UBlackboardKeyType_Object>(BlackboardKey_TargetActor, InTargetActor);
	}
	TargetActorCached = InTargetActor;

	SetFocus(InTargetActor, EAIFocusPriority::Default);
}
