// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GenericTeamAgentInterface.h"
#include "SandboxTeamActorInterface.generated.h"

/** Teams for players, monsters, spectators etc */
UENUM(BlueprintType)
namespace ESandboxTeam
{
	enum Type
	{
		Spectator,
		RedTeam,
		BlueTeam,
		MAX, // this should always be last.
	};
}

namespace ESandboxTeam
{
	/** @return version of the enum passed in as string */
	inline const TCHAR* ToString(ESandboxTeam::Type Team)
	{
		switch (Team)
		{
		case RedTeam:
		{
			return TEXT("RedTeam");
		}
		case BlueTeam:
		{
			return TEXT("BlueTeam");
		}
		case Spectator:
		{
			return TEXT("Spectator");
		}
		}
		return TEXT("No Team Assignment");
	}
}

/** Interface for actors which can be associated with teams */
UINTERFACE()
class USandboxTeamActorInterface : public UGenericTeamAgentInterface
{
	GENERATED_UINTERFACE_BODY()
};

class ISandboxTeamActorInterface : public IGenericTeamAgentInterface
{
	GENERATED_IINTERFACE_BODY()

	UFUNCTION(Category = "Team")
	virtual TEnumAsByte<ESandboxTeam::Type> GetTeam() const = 0;

	/** Get a human readable name for the team */
	FString ToDebugString() { return ESandboxTeam::ToString(GetTeam()); }

	virtual FGenericTeamId GetGenericTeamId() const override;
	virtual ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;
	virtual ETeamAttitude::Type GetTeamAttitudeTowardsTeam(ESandboxTeam::Type OtherTeam) const;
};
