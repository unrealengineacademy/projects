// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "SandboxCharacter.generated.h"

UCLASS(Blueprintable)
class ASandboxCharacter : public ACharacter
{
	GENERATED_BODY()


public:
	ASandboxCharacter();
};

