// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Sandbox.h"
#include "SandboxTeamActorInterface.h"

USandboxTeamActorInterface::USandboxTeamActorInterface(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

FGenericTeamId ISandboxTeamActorInterface::GetGenericTeamId() const
{
	return FGenericTeamId(GetTeam());
}

ETeamAttitude::Type ISandboxTeamActorInterface::GetTeamAttitudeTowards(const AActor& Other) const
{
	const ISandboxTeamActorInterface* TeamActorB = Cast<const ISandboxTeamActorInterface>(&Other);
	if (TeamActorB == nullptr && Cast<APawn>(&Other) != nullptr && Cast<APawn>(&Other)->Controller != nullptr)
	{
		TeamActorB = Cast<const ISandboxTeamActorInterface>(Cast<APawn>(&Other)->Controller);
	}

	if (TeamActorB)
	{
		const ESandboxTeam::Type OtherTeam = TeamActorB->GetTeam();

		return GetTeamAttitudeTowardsTeam(OtherTeam);
	}

	return ETeamAttitude::Neutral;
}

ETeamAttitude::Type ISandboxTeamActorInterface::GetTeamAttitudeTowardsTeam(ESandboxTeam::Type OtherTeam) const
{
	const ESandboxTeam::Type TeamA = GetTeam();
	const ESandboxTeam::Type TeamB = OtherTeam;

	return (TeamA == TeamB) ? ETeamAttitude::Friendly
		: ((TeamA == ESandboxTeam::Spectator || TeamB == ESandboxTeam::Spectator) ? ETeamAttitude::Neutral
		: ETeamAttitude::Hostile);
}