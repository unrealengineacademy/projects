// Copyright 2015 Sebastian Kowalczyk, Inc. All Rights Reserved.

#pragma once
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SandboxBPLibrary.generated.h"

UCLASS()
class SANDBOX_API USandboxBPLibrary : public UBlueprintFunctionLibrary
{
public:
	GENERATED_BODY()

	//UFUNCTION(BlueprintPure, Category = "Sandbox|DefaultObject")
	//static FVector ExtractSkeletalMeshScale(class UClass* Class);

	//UFUNCTION(BlueprintPure, Category = "Sandbox|DefaultObject")
	//static USkeletalMesh* ExtractSkeletalMesh(class UClass* Class);
};
