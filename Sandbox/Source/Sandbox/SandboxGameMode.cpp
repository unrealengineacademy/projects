// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Sandbox.h"
#include "SandboxGameMode.h"
#include "SandboxPlayerController.h"
#include "SandboxCharacter.h"

ASandboxGameMode::ASandboxGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASandboxPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}