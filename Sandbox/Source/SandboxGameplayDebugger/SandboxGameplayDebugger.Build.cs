// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SandboxGameplayDebugger : ModuleRules
{
	public SandboxGameplayDebugger(TargetInfo Target)
	{
		PrivateIncludePaths.Add("../Sandbox");
        PrivateIncludePaths.Add("../Sandbox/AI");


		PublicDependencyModuleNames.AddRange(
			new string[] {
				"GameplayDebugger",
			}
		);

		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"CoreUObject",
                "InputCore",
				"Engine",    
                "RenderCore",
                "RHI",
                "Sandbox",
                "AIModule",
               	"GameplayAbilities",
			}
		);

        CircularlyReferencedDependentModules.Add("Sandbox");

        if (UEBuildConfiguration.bCompileMcpOSS == true)
        {
            PrivateDependencyModuleNames.AddRange(
                new string[]
				{
					"OnlineSubsystemMcp",
				}
            );
        }
		
		// Disable shadow variable warnings
		bEnableShadowVariableWarnings = false;
    }
}
