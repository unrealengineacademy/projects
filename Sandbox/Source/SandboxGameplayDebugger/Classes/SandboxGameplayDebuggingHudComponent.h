// Copyright 2002-2015 People Can Fly, Inc. All Rights Reserved.
#pragma once

#include "Engine/Canvas.h"
#include "GameplayDebuggingHUDComponent.h"
#include "SandboxGameplayDebuggingHudComponent.generated.h"

UCLASS(notplaceable, MinimalAPI)
class ASandboxGameplayDebuggingHudComponent : public AGameplayDebuggingHUDComponent
{
    GENERATED_UCLASS_BODY()

protected:
	virtual void DrawBasicData(APlayerController* PC, class UGameplayDebuggingComponent *DebugComponent) override;
};

