// Copyright 2002-2015 People Can Fly, Inc. All Rights Reserved.
#pragma once

#include "GameplayDebuggingComponent.h"
#include "SandboxGameplayDebuggingComponent.generated.h"

UCLASS(MinimalAPI)
class USandboxGameplayDebuggingComponent : public UGameplayDebuggingComponent
{
public:
    GENERATED_UCLASS_BODY()
    virtual void CollectBasicData() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

    UPROPERTY(Replicated)
    FString TeamName; //custom data replicated to clients
};