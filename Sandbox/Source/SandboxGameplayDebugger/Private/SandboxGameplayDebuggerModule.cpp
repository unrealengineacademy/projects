// Copyright 2002-2015 People Can Fly, Inc. All Rights Reserved.
#include "SandboxGameplayDebugger.h"
#include "SandboxGameplayDebuggerModule.h"

class FSandboxGameplayDebuggerModule : public ISandboxGameplayDebuggerModule
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override
	{
	}
};

IMPLEMENT_MODULE(FSandboxGameplayDebuggerModule, SandboxGameplayDebugger)

