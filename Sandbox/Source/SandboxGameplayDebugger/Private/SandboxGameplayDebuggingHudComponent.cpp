
// Copyright 2002-2015 People Can Fly, Inc. All Rights Reserved.
#include "SandboxGameplayDebugger.h"
#include "GameplayDebuggingReplicator.h"
#include "SandboxGameplayDebuggingComponent.h"
#include "SandboxGameplayDebuggingHudComponent.h"

ASandboxGameplayDebuggingHudComponent::ASandboxGameplayDebuggingHudComponent(const class FObjectInitializer& PCIP)
    : Super(PCIP)
{
}

void ASandboxGameplayDebuggingHudComponent::DrawBasicData(APlayerController* PC, class UGameplayDebuggingComponent *DebugComponent)
{
    Super::DrawBasicData(PC, DebugComponent);
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	const USandboxGameplayDebuggingComponent* MyComp = Cast<USandboxGameplayDebuggingComponent>(DebugComponent);
    if (MyComp)
    {
		PrintString(DefaultContext, FString::Printf(TEXT("{white}Team: {red}%s\n"), *MyComp->TeamName));
    }
#endif
}
