
// Copyright 2002-2015 People Can Fly, Inc. All Rights Reserved.
#include "SandboxGameplayDebugger.h"
#include "Net/UnrealNetwork.h"
#include "GameplayDebuggingComponent.h"
#include "GameplayDebuggingReplicator.h"
#include "SandboxGameplayDebuggingComponent.h"
#include "../../Sandbox/AI/SandboxAICharacter.h"
#include "../../Sandbox/AI/SandboxAIController.h"

USandboxGameplayDebuggingComponent::USandboxGameplayDebuggingComponent(const class FObjectInitializer& PCIP) : Super(PCIP) { }

void USandboxGameplayDebuggingComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps( OutLifetimeProps );
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	DOREPLIFETIME(USandboxGameplayDebuggingComponent, TeamName);
#endif
}

void USandboxGameplayDebuggingComponent::CollectBasicData()
{
	if (GetSelectedActor())
	{
		Super::CollectBasicData();
	}
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	ASandboxAICharacter* MyPawn = Cast<ASandboxAICharacter>(GetSelectedActor());
	ASandboxAIController* MyAI = MyPawn ? Cast<ASandboxAIController>(MyPawn->GetController()) : nullptr;
	if (MyAI)
	{
		TeamName = ESandboxTeam::ToString(MyAI->GetTeam());
	}
#endif
}

void USandboxGameplayDebuggingComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!GetSelectedActor())
	{
		CollectBasicData();
	}
}