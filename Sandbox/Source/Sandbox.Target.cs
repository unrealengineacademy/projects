// Copyright 2015 Sebastian Kowalczyk, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class SandboxTarget : TargetRules
{
	public SandboxTarget(TargetInfo Target)
	{
		Type = TargetType.Game;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.Add("Sandbox");
        if (Target.Configuration != UnrealTargetConfiguration.Shipping && Target.Configuration != UnrealTargetConfiguration.Test)
        {
            OutExtraModuleNames.Add("SandboxGameplayDebugger");
        }

		if (UEBuildConfiguration.bBuildEditor)
		{
			OutExtraModuleNames.Add("SandboxEditor");
		}
	}
}
