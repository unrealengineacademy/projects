// Copyright 2015 Sebastian Kowalczyk, Inc. All Rights Reserved.

/**
 * Sandbox-specific editor functionality
 */

#include "SandboxUnrealEdEngine.generated.h"

UCLASS()
class USandboxUnrealEdEngine : public UUnrealEdEngine
{
	GENERATED_UCLASS_BODY()

	/* Hook up specific callbacks */
	virtual void Init(IEngineLoop* InEngineLoop) override;
	virtual void InitializeObjectReferences() override;

	/* Clear callbacks */
	virtual void FinishDestroy() override;

	/* Handles fixup code before a world is saved */
	virtual void OnPreSaveWorld( uint32 SaveFlags, class UWorld* World ) override;

	/** Handles random yaw and scale of BuildingSMActors */
	virtual void HandleNewActorsDropped(const TArray<UObject*>& DroppedObjects, const TArray<AActor*>& DroppedActors);

	virtual void PlayInEditor( UWorld* InWorld, bool bInSimulateInEditor );
};
