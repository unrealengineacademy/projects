// Copyright 2015 Sebastian Kowalczyk, Inc. All Rights Reserved.

#pragma once

#include "UnrealEd.h"
#include "Engine.h"
#include "OnlineSubsystem.h"
#include "GameplayTagContainer.h"
#include "ParticleDefinitions.h"
#include "BlueprintUtilities.h"
#include "SandboxEditorClasses.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSandboxEditor, Log, All);
