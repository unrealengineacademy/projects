// Copyright 2015 Sebastian Kowalczyk, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SandboxEditor : ModuleRules
{
	public SandboxEditor(TargetInfo Target)
	{
		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"AssetRegistry",
				"AssetTools",
				"ClassViewer",
				"MainFrame",
			}
		);

		PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"CoreUObject",
                "InputCore",
				"Engine",
				"Slate",
                "EditorStyle",
				"UnrealEd",
				"Sandbox",                
                "AIModule",
				"UMG",
			}
		);

		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"AppFramework",
				"RenderCore",
				"RHI",
				"OnlineSubsystem",
				"SourceControl",
				"PropertyEditor",
				"StaticMeshEditor",
				"BlueprintGraph",
				"KismetCompiler",
				"HTTP",
				"GraphEditor",
				"KismetWidgets",
				"GameplayTags",
				"GameplayTagsEditor",
				"Json",
                "JsonUtilities",
				"SlateCore",
                "GameplayTasks",
                "GameplayTasksEditor",
                "GameplayAbilities",
                "GameplayAbilitiesEditor",
				"GameProjectGeneration",
				"MovieScene",
				"UMGEditor",
			}
		);

		DynamicallyLoadedModuleNames.AddRange(
			new string[] {
				"AssetRegistry",
				"AssetTools",
				"ClassViewer",
				"MainFrame"
			}
		);

		// Add TargetPlatform modules for non-windows platforms we care about (Mac)
		DynamicallyLoadedModuleNames.Add("MacClientTargetPlatform");
		
		// Disable shadow variable warnings
		bEnableShadowVariableWarnings = false;
	}
}
