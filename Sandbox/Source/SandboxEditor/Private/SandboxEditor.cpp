// Copyright 2015 Sebastian Kowalczyk, Inc. All Rights Reserved.

#include "SandboxEditor.h"
#include "LevelEditor.h"

#define LOCTEXT_NAMESPACE "SandboxEditor"

DEFINE_LOG_CATEGORY(LogSandboxEditor);

class FSandboxEditorModule : public FDefaultGameModuleImpl
{
	virtual void StartupModule() override
	{
		//FSandboxEditorStyle::Initialize();
	}

	virtual void ShutdownModule() override
	{
		//FSandboxEditorStyle::Shutdown();
	}
};


IMPLEMENT_GAME_MODULE(FSandboxEditorModule, SandboxEditor);


static void AddFiles(TArray<FString>& Filenames, const TCHAR* Dir, const TCHAR* Wildcard)
{
	TArray<FString> Files;
	IFileManager::Get().FindFilesRecursive(Files, *(FPaths::GameContentDir() / Dir), Wildcard, true, false);
	for (int32 Index = 0; Index < Files.Num(); Index++)
	{
		FString StdFile = Files[Index];
		FPaths::MakeStandardFilename(StdFile);
		Filenames.AddUnique(StdFile);
	}
}

#undef LOCTEXT_NAMESPACE
