// Copyright 2015 Sebastian Kowalczyk, Inc. All Rights Reserved.

#include "SandboxEditor.h"

#define LOCTEXT_NAMESPACE "SandboxEditor"

USandboxUnrealEdEngine::USandboxUnrealEdEngine(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void USandboxUnrealEdEngine::Init(IEngineLoop* InEngineLoop)
{
	Super::Init(InEngineLoop);

	if (!HasAnyFlags(RF_ClassDefaultObject))
	{
		// Register events
		FEditorDelegates::OnNewActorsDropped.AddUObject(this, &USandboxUnrealEdEngine::HandleNewActorsDropped);
	}
}

void USandboxUnrealEdEngine::InitializeObjectReferences()
{
	Super::InitializeObjectReferences();
}

void USandboxUnrealEdEngine::FinishDestroy()
{
	if ( !HasAnyFlags(RF_ClassDefaultObject) )
	{
		// Unregister events
		FEditorDelegates::OnNewActorsDropped.RemoveAll(this);
	}

	Super::FinishDestroy();
}

void USandboxUnrealEdEngine::OnPreSaveWorld(uint32 SaveFlags, UWorld* World)
{
	Super::OnPreSaveWorld(SaveFlags, World);

	const bool bAutosaveOrPIE = (SaveFlags & SAVE_FromAutosave) != 0;
}

void USandboxUnrealEdEngine::HandleNewActorsDropped(const TArray<UObject*>& DroppedObjects, const TArray<AActor*>& DroppedActors)
{

}


void USandboxUnrealEdEngine::PlayInEditor(UWorld* InWorld, bool bInSimulateInEditor)
{
	Super::PlayInEditor(InWorld, bInSimulateInEditor);
}



#undef LOCTEXT_NAMESPACE
